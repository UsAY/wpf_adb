﻿using AndroidBridge.ViewModel;
using MvvmCross.Wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAndroid.Views
{
    /// <summary>
    /// Логика взаимодействия для DeviceInfoView.xaml
    /// </summary>
    public partial class DeviceInfoView : MvxWpfView
    {
        public DeviceInfoView()
        {
            InitializeComponent();
            
        }
        public new DeviceInfoViewModel ViewModel
        {
            get { return (DeviceInfoViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
    }
}
