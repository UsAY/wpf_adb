// --------------------------------------------------------------------------------------------------------------------
// <summary>
//    Defines the BaseView type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace WpfAndroid.Views
{
    using MvvmCross.Wpf.Views;

    /// <summary>
    ///  Defines the BaseView type.
    /// </summary>
    public abstract class BaseView : MvxWpfView
    {
    }
}
