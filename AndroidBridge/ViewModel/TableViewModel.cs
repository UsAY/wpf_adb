﻿using AndroidBridge.Model;
using Microsoft.Win32;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace AndroidBridge.ViewModel
{
   public class TableViewModel: BaseViewModel
    {

        const string REGISTRY_KEY = @"HKEY_CURRENT_USER\" + "AppName";
        const string REGISTY_VALUE = "FirstRun";
        const string REGISTRY_PATH_ADB = "ADB";
        const string APPVersion = "1";
        const string APPName = "WPFANDROID";
        private readonly string _PATH_ADB;

        private readonly AndroidBridge _androidBridge;
        private List<DeviceInformation> _deviceInformation;
        public List<DeviceInformation> DeviceInformationList
        {
            get { return _deviceInformation; }
            set { _deviceInformation = value;
                RaisePropertyChanged(()=>DeviceInformationList);
            }
        }

        public TableViewModel()
        {
             isFirstRun();
            _PATH_ADB = GetPath();
            _androidBridge =new AndroidBridge(_PATH_ADB);
            _androidBridge.DeviceUpdate += (s, e) => UpdateList();
            UpdateList();
        }
        public override void Destroy()
        {
            _androidBridge.DeviceUpdate -= (s, e) => UpdateList();
        }

        private DeviceInformation _selectedDevice;
      public DeviceInformation SelectedDevice {
            get { return _selectedDevice; }
            set { _selectedDevice = value;
                _androidBridge.DeviceUpdate -= (s, e) => UpdateList();
                ShowViewModel<DeviceInfoViewModel>(_selectedDevice);
            }
        }

        private IMvxCommand _clickUpdate;
        public IMvxCommand ClickUpdate
        {
            get
            {
                return _clickUpdate ?? (_clickUpdate = new MvxCommand(UpdateList));
            }
        }

       

        public void UpdateList()
        {
            DeviceInformationList = _androidBridge.GetAndroidDevices().ToList();
        }

        private bool isFirstRun()
        {
            const string REGISTRY_KEY = @"HKEY_CURRENT_USER\"+APPName;
            const string REGISTY_VALUE = "FirstRun";
            
            if (Convert.ToInt32(Microsoft.Win32.Registry.GetValue(REGISTRY_KEY, REGISTY_VALUE, 0)) == 0)
            {
                string folderPath = "";
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                MessageBox.Show("При первом запуске нужно выбрать директорию где находится ADB");
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    folderPath = folderBrowserDialog.SelectedPath;
                    SavePath(folderPath);
                    var str = GetPath();
                }
                else { return false; }
                Microsoft.Win32.Registry.SetValue(REGISTRY_KEY, REGISTY_VALUE, 1, Microsoft.Win32.RegistryValueKind.DWord);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SavePath(string Path)
        {
            RegistryKey key = GetKey();

            key.SetValue(REGISTRY_PATH_ADB, Path);
        }
        private RegistryKey GetKey()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);
            key.CreateSubKey(APPName);
            key = key.OpenSubKey(APPName, true);
            key.CreateSubKey(APPVersion);
            key = key.OpenSubKey(APPVersion, true);
            return key;
        }

        private string GetPath()
        {
            RegistryKey key= GetKey();

           string path=(string) key.GetValue(REGISTRY_PATH_ADB, "");
            return path;
        }



    }

}
