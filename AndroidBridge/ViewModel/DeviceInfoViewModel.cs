﻿using AndroidBridge.Model;
using AndroidBridge.ViewModel;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndroidBridge.ViewModel
{
   public class DeviceInfoViewModel: BaseViewModel
    {
        private DeviceInformation deviceInfo;
       ObservableCollection<KeyValuePair<String, double>> _diskUsage ;
      public ObservableCollection<KeyValuePair<String, double>> DiskUsage
        {
            get { return _diskUsage; }
            set { _diskUsage = value;
                RaisePropertyChanged(()=>DiskUsage);
            }

        }
        public DeviceInfoViewModel() {
            DiskUsage = new ObservableCollection<KeyValuePair<string, double>>();
        }
        public void Init(DeviceInformation DI)
        {
            Serial = DI.SerialNum;
            Bat = DI.BatState+0.0d;
            DeviceTipe = DI.IsDevice ? "Устройство" : "Эмулятор";
            ModelDevice = DI.DeviceModel;
            VersionOS = DI.OsVersion;
            DiskUsage.Add(new KeyValuePair<string, double>("Использовано "+ DI.UsageMemory, DI.UsageMemory));
            DiskUsage.Add(new KeyValuePair<string, double>("Свободно "+ DI.FreeMemory, DI.FreeMemory));
         //   RaisePropertyChanged(() => DiskUsage);
        }

       

        private string _serial;
        public string Serial { get { return _serial; }

        set { _serial ="Серийный номер: "+ value;
                RaisePropertyChanged(()=>Serial);
            }
        }

        private string _deviceType;
        public string DeviceTipe {
            get { return _deviceType; }
            set {
                _deviceType = "Тип устройства: "+value;
                RaisePropertyChanged(() => DeviceTipe);
            }
        }

        private string _model;
        public string ModelDevice { get { return _model; }
            set { _model ="Модель: "+ value;
                RaisePropertyChanged(() => ModelDevice);
            }
        }

        private string _versionOS;
        public string VersionOS { get { return _versionOS; }
            set { _versionOS = "Версия ОС: "+value; }
        }

        private IMvxCommand _clickBack;
        public IMvxCommand ClickBack
        {
            get
            {
                return _clickBack ?? (_clickBack = new MvxCommand(BackWard));
            }
        }

        private void BackWard()
        {
            ShowViewModel<TableViewModel>();
        }

        double _bat;
        public double Bat { get { return _bat; }

        set { _bat = value;
                RaisePropertyChanged(()=>Bat);
                RaisePropertyChanged(() => BatStr);
            }
        }

        public string BatStr { get { return "Уровень заряда: "+_bat + "%"; } }

    }

   
}
