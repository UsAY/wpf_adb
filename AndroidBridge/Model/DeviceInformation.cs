﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidBridge.Model;
using Managed.Adb;

namespace AndroidBridge.Model
{
    public class DeviceInformation
    {
      public  string SerialNum { get; set; }
      public bool IsDevice { get; set; }

        public string DeviceModel { get; set; }
        public string OsVersion { get; set; }
        public int BatState { get; set;}

        public DiskStats DiskState { get; set; }

        public double FreeMemory { get; set; }
        public double UsageMemory { get; set; }
       
    }
}
