﻿using Managed.Adb;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndroidBridge.Model
{
  public  class DiskStats
    {
        /// <summary>
        /// All device memory in GB
        /// </summary>
        public double AllMemory { get; set; }
        /// <summary>
        /// Use memory in Gb
        /// </summary>
        public double UseMemory { get; set; }
        /// <summary>
        /// Free memory in Gb
        /// </summary>
        public double FreeMemory { get; set; }

        public DiskStats()
        { }

        public DiskStats(Device device)
        {
            DiskStats MemoryState = GetDeviceMemory(device);
            AllMemory = MemoryState.AllMemory;
            UseMemory = MemoryState.UseMemory;
            FreeMemory = MemoryState.FreeMemory;
        }


        public DiskStats GetDeviceMemory(Device device)
        {
            DiskStats ds = new DiskStats();
            Managed.Adb.IShellOutputReceiver reciver = new Managed.Adb.CommandResultReceiver();
            device.ExecuteShellCommand("df", reciver);
            var str = ((Managed.Adb.CommandResultReceiver)reciver).Result;
            String defaultsd = device.GetProperty("persist.sys.sd.defaultpath");
            var str_arr = str.Split(new string[] { defaultsd }, 2, StringSplitOptions.RemoveEmptyEntries).Last();
            var sz = str_arr.Split(new char[] { ' ' }, 4, StringSplitOptions.RemoveEmptyEntries);
            if (sz.Length < 3) return ds;
            ds.AllMemory = GetMemoryFromString(sz[0]);
            ds.UseMemory = GetMemoryFromString(sz[1]);
            ds.FreeMemory = GetMemoryFromString(sz[2]);
            return ds;
        }

        private double GetMemoryFromString(string memory_string)
        {
            string MemoryVal = memory_string.Substring(0, memory_string.Length - 1);
            string MemoryUnit = memory_string.Substring(memory_string.Length - 1);

            double Value = 0d;
            bool parseRes=Double.TryParse(MemoryVal, NumberStyles.Any, new CultureInfo("en-US"), out Value);
            if (!parseRes) return Value;
            if (MemoryUnit.Equals("K")) Value = Value / 1024 / 1024;
            else if (MemoryUnit.Equals("M")) Value = Value / 1024;

            return Value;
        }
    }
}
