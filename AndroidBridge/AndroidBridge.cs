﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managed.Adb;
using System.IO;
using AndroidBridge.Model;


namespace AndroidBridge
{
    public class AndroidBridge
    {
        protected readonly AndroidDebugBridge _bridge;
      

        public event EventHandler DeviceUpdate;
        public AndroidBridge(string path)
        {
           
            _bridge = AndroidDebugBridge.CreateBridge(Path.Combine(path, "adb.exe"), true);
            
            _bridge.Start();
          
         
            _bridge.DeviceConnected +=(s,e)=>  MonitorDevice();
            _bridge.DeviceDisconnected += (s, e) => MonitorDevice();
            GetAndroidDevices();
        }

        private void MonitorDevice()
        {
            if (DeviceUpdate != null) DeviceUpdate(null,EventArgs.Empty);
        }

        public IEnumerable<DeviceInformation> GetAndroidDevices()
        {
            List<DeviceInformation> DeviceList = new List<DeviceInformation>();
          
        
            try
            {
                foreach (var dev in AdbHelper.Instance.GetDevices(AndroidDebugBridge.SocketAddress))
                {
                    DeviceInformation deviceInfoemation = new DeviceInformation();
                    deviceInfoemation.IsDevice = !dev.IsEmulator;
                    deviceInfoemation.SerialNum = dev.SerialNumber;
                    deviceInfoemation.BatState = dev.GetBatteryInfo().Level;
                    deviceInfoemation.DeviceModel = dev.Model;
                    deviceInfoemation.OsVersion = dev.GetProperty("ro.build.version.release"); 
                    deviceInfoemation.DiskState = new DiskStats(dev);
                    deviceInfoemation.FreeMemory = deviceInfoemation.DiskState.FreeMemory;
                    deviceInfoemation.UsageMemory = deviceInfoemation.DiskState.UseMemory;
                    DeviceList.Add(deviceInfoemation);
                }
                return DeviceList;
            }
            catch (Exception ex)
            {
                
                Console.WriteLine("ошибка при формировании списка. возможны не допустимые символы в имени устройства");
                return DeviceList;
            }
          
      
        }
    }
}
